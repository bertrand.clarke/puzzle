//
//  Parameters.m
//  puzzle
//
//  Created by Bertrand Clarke on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Parameters.h"
#import "MainMenuScene.h"

static NSNumber *minPointsPerPiece;
static unsigned int borderWidth;
static CGColorRef borderColor;
static BOOL showBackground;
static BOOL showGrid;
static BOOL rotatePieces;

@implementation Parameters

+(void)loadParametersFromFile
{
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath = [[MainMenuScene mainMenuScene].currentProductPath stringByAppendingPathComponent:@"Parameters.plist"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        NSLog(@"Error : missing file : %@",plistPath);
    }
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSDictionary *parameters = (NSDictionary *)[NSPropertyListSerialization
                                          propertyListFromData:plistXML
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];
    if (!parameters) {
        NSLog(@"Error reading plist: %@, format: %d", errorDesc, format);
    }

    minPointsPerPiece = [parameters objectForKey:@"minPointsPerPiece"];
    borderWidth = [[parameters objectForKey:@"borderWidth"] unsignedIntValue];
    NSArray *borderColorComponents = [parameters objectForKey:@"borderColor"];
    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
    CGFloat components [4] = {[[borderColorComponents objectAtIndex:0] floatValue], [[borderColorComponents objectAtIndex:1] floatValue], [[borderColorComponents objectAtIndex:2] floatValue], [[borderColorComponents objectAtIndex:3] floatValue]};
    borderColor = CGColorCreate(rgb, components);
    CGColorSpaceRelease(rgb);
    showBackground = [[parameters objectForKey:@"showBackground"] boolValue];
    showGrid = [[parameters objectForKey:@"showGrid"] boolValue];
    rotatePieces = [[parameters objectForKey:@"rotatePieces"] boolValue];
}

+(unsigned int)borderWidth
{
    return borderWidth + 3;
}

+(CGColorRef)borderColor
{
    return borderColor;
}

+(NSNumber *)minPointsPerPiece
{
    return minPointsPerPiece;
}
+(BOOL)showBackground
{
    return showBackground;
}
+(BOOL)showGrid
{
    return showGrid;
}
+(BOOL)rotatePieces
{
    return rotatePieces;
}
@end

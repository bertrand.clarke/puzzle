//
//  ModelLoader.h
//  puzzle
//
//  Created by Bertrand Clarke on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PuzzleScene.h"

@interface ModelLoader : NSObject
{
    PuzzleScene *controller;
    unsigned char *rawData;
    unsigned char *gridData;
    CGContextRef gridContextRef;
    size_t imageWidth;
    size_t imageHeight;
    size_t bitsPerComponent;
    size_t bytesPerRow;
    unsigned int bytesPerPixel;
    CGColorSpaceRef colorSpace;
    CGImageRef backgroundRef;
    CGSize winSize;
//    CGFloat scale;
    // color -> PieceRect 
    NSMutableDictionary *piecesColorOrigin;
    unsigned char pieceRed;
    unsigned char pieceGreen;
    unsigned char pieceBlue;
    CGRect pieceRect;
    CGImageRef currentImageRef;
    NSString *pieceImagePath;
    NSString *pieceWithBorderImagePath;
    BOOL generatePieces;
}

- (void)loadModelWithController:(PuzzleScene *)puzzleLayer;
+(NSString *)storingPath:(NSString *)productName level:(NSNumber *)level filename:(NSString *)filename;

@end

//
//  DrawBorder.m
//  puzzle
//
//  Created by Bertrand Clarke on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Border.h"
#import "BorderPoint.h"
#include "Parameters.h"

@implementation Border

-(id)initWithSize:(CGSize)s andBytesPerPixel:(unsigned int)b
{
    self = [super init];
    if(self)
    {
        size = s;
        bytesPerPixel = b;
        borderWidth = [Parameters borderWidth];
        borderColor = malloc(sizeof(unsigned char) * 4);
        const CGFloat *components = CGColorGetComponents([Parameters borderColor]);
        for(int i = 0;i<4;++i)
        {
            borderColor[i] = (int)components[i] * 255.0;
        }
    }
    return self;
}

-(void)colorPoint:(int)row col:(int)col color:(unsigned char [])color
{
    int byteIndex = ((row * size.width) + col) * bytesPerPixel;
    pieceData[byteIndex] = color[0];
    pieceData[byteIndex + 1] = color[1];
    pieceData[byteIndex + 2] = color[2];
    pieceData[byteIndex + 3] = color[3];
}

-(void)drawBorderIn:(CGContextRef)context
{
    pieceData = (unsigned char *)CGBitmapContextGetData (context);
    // Find border points
    NSMutableSet *borderPoints = [[NSMutableSet alloc] init];
    for(int row = 0; row < size.height ; ++row)
    {
        for(int col = 0; col < size.width ; ++col)
        {
            int byteIndex = ((row * size.width) + col) * bytesPerPixel;
            BorderPoint *bp = [[BorderPoint alloc] init];
            bp.row = row;
            bp.col = col;
            if(pieceData[byteIndex + 3] == 255) {
                if(row < size.height - 1)
                {
                    if(row > 0) 
                    {
                        int index = (((row - 1) * size.width) + col) * bytesPerPixel;
                        if(pieceData[index + 3] != 255) 
                        {
                            bp.isTopBorder = YES;
                        }
                    } else {
                        int index = ((row * size.width) + col) * bytesPerPixel;
                        if(pieceData[index + 3] != 0) 
                        {
                            bp.isTopBorder = YES;
                        }
                    }
                    if(row > 1 && row < size.height + 1)
                    {
                        int index = (((row + 1) * size.width) + col) * bytesPerPixel;
                        if(pieceData[index + 3] != 255) 
                        {
                            bp.isBottomBorder = YES;
                        }
                    }
                } else {
                    int index = ((row * size.width) + col) * bytesPerPixel;
                    if(pieceData[index + 3] != 0) 
                    {
                        bp.isBottomBorder = YES;
                    }
                }
                if(col < size.width - 1)
                {
                    if(col > 0) 
                    {
                        int index = (((row) * size.width) + col - 1) * bytesPerPixel;
                        if(pieceData[index + 3] != 255) 
                        {
                            bp.isLeftBorder = YES;
                        }
                    } else {
                        int index = ((row * size.width) + col) * bytesPerPixel;
                        if(pieceData[index + 3] != 0) 
                        {
                            bp.isLeftBorder = YES;
                        }
                    }
                    if(col > 1 && col < size.width - 1)
                    {
                        int index = (((row) * size.width) + col + 1) * bytesPerPixel;
                        if(pieceData[index + 3] != 255) 
                        {
                            bp.isRightBorder = YES;
                        }
                        
                    }
                } else {
                    int index = ((row * size.width) + col) * bytesPerPixel;
                    if(pieceData[index + 3] != 0) 
                    {
                        bp.isRightBorder = YES;
                    }
                }
                if(bp.isTopBorder || bp.isBottomBorder || bp.isLeftBorder || bp.isRightBorder)
                {
                    [borderPoints addObject:bp];
                }
            }
        }
    }
    
    CGColorRef color = [Parameters borderColor];
    CGContextSetFillColorWithColor(context, color);
    for(BorderPoint *bp in borderPoints) 
    {
//        if(bp.col >= 0) 
//        {
            CGContextFillRect(context, CGRectMake(bp.col, size.height - bp.row - 1, 1,1));
//        }
    }
    
}
@end
//
//  Utils.h
//  puzzle
//
//  Created by Bertrand Clarke on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define Log(s,...) [Utils log:__FILE__ lineNumber:__LINE__ format:(s),##__VA_ARGS__]

@interface Utils : NSObject
+ (void)log:(char*)sourceFile lineNumber:(int)lineNumber format:(NSString*)format, ...;
+ (void)logError:(NSError *)error;
+ (BOOL)iPad;
+ (void)setRetinaDisplay:(BOOL)retina;
+ (BOOL)retinaDisplay;
+(NSString *)filenameWithBase:baseFilename andExtension:extension forWriting:(BOOL)writing;
@end

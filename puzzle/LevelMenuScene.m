//
//  LevelMenuScene.m
//  puzzle
//
//  Created by Bertrand Clarke on 05/06/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "LevelMenuScene.h"
#import "MainMenuScene.h"
#import "SimpleAudioEngine.h"

@implementation LevelMenuScene

+(CCScene *) scene
{
    // 'scene' is an autorelease object.
    CCScene *scene = [CCScene node];
    
    // 'layer' is an autorelease object.
    LevelMenuScene *layer = [LevelMenuScene node];
    
    // add layer as a child to scene
    [scene addChild: layer];
    
    // return the scene
    return scene;
}


// on "init" you need to initialize your instance
-(id) init
{
    if( (self=[super init])) {
        self.isTouchEnabled = YES;
        
        CCMenu * exitMenu = [[CCMenu alloc] initWithItems:nil vaList:nil];
        // Add top left return button 
        CCMenuItemImage *exitMenuItem = [CCMenuItemImage itemFromNormalImage:@"exit.png" selectedImage:@"exitSelected.png" 
                                                                 target:self selector:@selector(exit:)];
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        exitMenu.position = ccp(exitMenuItem.boundingBox.size.width, winSize.height - exitMenuItem.boundingBox.size.height);
        
        [exitMenu addChild:exitMenuItem];
        
        [self addChild:exitMenu];
        
        CCMenu * levelsMenu = [[CCMenu alloc] initWithItems:nil vaList:nil];
        for(NSNumber *number in [MainMenuScene mainMenuScene].levels)
        {
            CCMenuItemImage *menuItem = [CCMenuItemImage 
                                         itemFromNormalImage:@"piecesChoice.png" selectedImage:@"piecesChoiceSelected.png" 
                                         target:self selector:@selector(start:)];
            menuItem.tag = [number intValue];
            [levelsMenu addChild:menuItem];
            
        }
        
        // Arrange the menu items vertically
        [levelsMenu alignItemsVertically];
        [self addChild:levelsMenu z:0];
       
        // Add labels on buttons
        for(CCMenuItemImage *menuItem in levelsMenu.children)
        {
            CCLabelTTF *label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%i pièces", menuItem.tag] fontName:@"Baskerville-BoldItalic" fontSize:20];
            label.color = ccc3(0,0,0);
            label.position =  ccp(menuItem.position.x + levelsMenu.position.x, menuItem.position.y + levelsMenu.position.y);
            [self addChild: label z:1];
        }
        
    }
    return self;
}

- (void) start:(id)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"click.mp3"];
    [[CCDirector sharedDirector] popScene];
    [MainMenuScene mainMenuScene].currentLevel = [NSNumber numberWithInt:[sender tag]];
    [[MainMenuScene mainMenuScene] load];
}

-(void)exit:(id)sender
{
    [[MainMenuScene mainMenuScene] resetCurrentProductPath];
    [[SimpleAudioEngine sharedEngine] playEffect:@"click.mp3"];
    [[CCDirector sharedDirector] popScene];
}
@end

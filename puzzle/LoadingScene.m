//
//  LoadingScene.m
//  puzzle
//
//  Created by Bertrand Clarke on 04/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoadingScene.h"

@implementation LoadingScene

+(CCScene *) scene
{
    // 'scene' is an autorelease object.
    CCScene *scene = [CCScene node];
    
    // 'layer' is an autorelease object.
    LoadingScene *layer = [LoadingScene node];
    
    // add layer as a child to scene
    [scene addChild: layer];
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
    if( (self=[super init])) {
        // create and initialize a Label
        CCLabelTTF *label = [CCLabelTTF labelWithString:@"Chargement ..." fontName:@"Marker Felt" fontSize:64];
        CGSize size = [[CCDirector sharedDirector] winSize];
		label.position =  ccp( size.width /2 , size.height/2 );
		[self addChild: label];
    }
    return self;
}
@end

#!/bin/sh

#  ImportGeneratedFiles.sh
#  
#
#  Created by Bertrand Clarke on 13/06/12.
#  Copyright (c) 2012 Bcdlog. All rights reserved.

if [ $# -ne 2 ]
then
	echo "Copy all generated files from given simulator directory to Puzzle application directory"
	echo "$0 <simulator dir>"
	ls -ltr "/Users/bcdlog/Library/Application Support/iPhone Simulator/5.1/Applications/"
else
	echo "Copying /Users/bcdlog/Library/Application Support/iPhone Simulator/5.1/Applications/$1/Documents/products to /Users/bcdlog/Documents/Puzzle/puzzle/."
	find /Users/bcdlog/Documents/Puzzle/puzzle/products -name generated -exec rm -rf {} \;
	cp -R "/Users/bcdlog/Library/Application Support/iPhone Simulator/5.1/Applications/$1/Documents/products" /Users/bcdlog/Documents/Puzzle/puzzle/.
fi  

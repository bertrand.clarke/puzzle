//
//  AppDelegate.h
//  puzzle
//
//  Created by Bertrand Clarke on 23/05/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;
@class MainMenuScene;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

-(void)addGestureRecognizersTo:(CCLayer *)layer;
-(void)removeGestureRecognizers;

@end

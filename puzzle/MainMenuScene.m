//
//  MainMenuScene.m
//  puzzle
//
//  Created by Bertrand Clarke on 05/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainMenuScene.h"
#import "LevelMenuScene.h"
#import "SlidingMenuGrid.h"
#import "Utils.h"
#import "SimpleAudioEngine.h"
#import "LoadingScene.h"
#import "PuzzleScene.h"
#import "AppDelegate.h"
#import "ModelLoader.h"

#define THUMBNAIL_WIDTH 70
#define THUMBNAIL_WIDTH_IPAD 120

@implementation MainMenuScene

static MainMenuScene *singleton_;

@synthesize productsDirectories,currentProductPath,levels,currentLevel;

+(MainMenuScene *)mainMenuScene
{
    return singleton_;
}

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainMenuScene *layer = [MainMenuScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(void)findProducts
{
    // Get puzzles directories from Resources and NSDocument 
    
//    NSString *model;
//    if([Utils iPad])
//    {
//        model = @"iPad";
//    } else {
//        model = @"iPhone";
//    }
    NSString *productsPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"products"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    productsDirectories = [[NSMutableArray alloc] init];
    
//    int i =0;
    for(NSString *path in [fileManager subpathsAtPath:productsPath])
    {
        NSRange aRange = [path rangeOfString:@"generated"];
        if (aRange.location == NSNotFound)
        {
            NSString *fullPath = [productsPath stringByAppendingPathComponent:path];
            BOOL isDir=NO;
            if ([fileManager fileExistsAtPath:fullPath isDirectory:&isDir] && isDir)
            {
                NSLog(@"Found product %@", fullPath);
                [productsDirectories addObject:fullPath];
                
                //            CGImageRef bgIcon = [Utils thumbnailFromProductPath:fullPath].CGImage;
                //            CCSprite * item = [CCSprite spriteWithCGImage:bgIcon key:nil];
                //            item.position = ccp(320 +((i-1)*spacingBetweenLevels), 150);
                //            item.tag = i;
                //            [self addChild:item];
                //            [productImageSprites addObject:item];
                //            
                //            CCLabelTTF *label = [CCLabelTTF labelWithString:path fontName:@"Marker Felt" fontSize:20];
                //            label.position = item.position;
                //            [self addChild: label];
                //            [productNameLabels addObject:label];
                //            ++i;
            }
        }
    }
}


// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) {
						
        singleton_ = self;

#if TARGET_IPHONE_SIMULATOR
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0];
        NSString * directoryPath = [documentsPath stringByAppendingPathComponent:@"products"];
        NSLog(@"Documents directory = %@",directoryPath);
//        NSLog(@"Remove documents products directory");        
//        NSError *error;
//        [[NSFileManager defaultManager] removeItemAtPath:directoryPath error:&error];
#endif

        [self findProducts];
        int thumbWidth;
        if([Utils iPad])
        {
            thumbWidth = THUMBNAIL_WIDTH_IPAD;
        } else {
            thumbWidth = THUMBNAIL_WIDTH;
        }
		NSMutableArray* allItems = [NSMutableArray arrayWithCapacity:51];
		for (NSString* productPath in productsDirectories)
		{
            NSString *productName = [productPath lastPathComponent];
#if TARGET_IPHONE_SIMULATOR
            [MainMenuScene createDirectoryForProduct:productName level:nil];
#endif
            NSLog(@"Create icon for %@", productPath);
			// create a menu item for each product
            CGImageRef bgIcon = [self thumbnailFromProductPath:productPath width:thumbWidth].CGImage;            
			CCSprite* normalSprite = [CCSprite spriteWithCGImage:bgIcon key:nil];
            //CGImageRef bgIconSelected = [Utils thumbnailFromProductPath:productPath width:THUMBNAIL_WIDTH].CGImage;            
			//CCSprite* selectedSprite = [CCSprite spriteWithCGImage:bgIconSelected key:nil];
			CCMenuItemSprite* item =[CCMenuItemSprite itemFromNormalSprite:normalSprite selectedSprite:nil target:self selector:@selector(launchProduct:)];
			[allItems addObject:item];
		}
        CGPoint menuOrigin = CGPointMake(thumbWidth, thumbWidth);
		CGPoint padding = CGPointMake(thumbWidth, thumbWidth);
        CGSize winSize = [CCDirector sharedDirector].winSize;
        int cols = winSize.width / (thumbWidth + padding.x);
        int rows = winSize.height / (thumbWidth + padding.y);
		SlidingMenuGrid* menuGrid = [SlidingMenuGrid menuWithArray:allItems cols:cols rows:rows position:menuOrigin padding:padding verticalPages:NO];
		[self addChild:menuGrid];
	}
	return self;
}

-(void)findLevels
{
    levels = [[NSMutableArray alloc] init];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *files = [fileManager subpathsAtPath:currentProductPath];
    for(NSString *path in files)
    {
        // Get pieces-xx the number of pieces
        NSString *basename = [[path lastPathComponent] stringByDeletingPathExtension];
        if([basename hasPrefix:@"pieces-"] && ![basename hasPrefix:@"pieces-iPad"] && ![basename hasPrefix:@"pieces-hd"])
        {
            // Extract number
            NSNumber  *number = [NSNumber numberWithInteger: [[basename stringByReplacingOccurrencesOfString:@"pieces-" withString:@""] integerValue]];
            [levels addObject:number];
        }
    }
    
    // Sort levels ascending
    [levels sortUsingComparator:^(id obj1, id obj2) {
        NSNumber *n1 = obj1;
        NSNumber *n2 = obj1;
        if ([n1 intValue] > [n2 intValue])
            return NSOrderedAscending;
        else if ([n1 intValue] < [n2 intValue])
            return NSOrderedDescending;
        return NSOrderedSame;
    }];
}



// Sender is CCMenuItemSprite touched in the menu
- (void) launchProduct: (id) sender
{
    if(!currentProductPath)
    {
        CCMenuItemSprite *item = sender;
        [[SimpleAudioEngine sharedEngine] playEffect:@"click.mp3"];

        // If not calling findProducts again : BAD_ACCESS on productsDirectories
        [self findProducts];
        
        currentProductPath = [productsDirectories objectAtIndex:item.tag];
        

        [self findLevels];
        if([levels count] > 0)
        {
            [[CCDirector sharedDirector] pushScene:[LevelMenuScene scene]];
        } else {
            [self load];
        }
    }

}

-(void)load
{
    [[CCDirector sharedDirector] pushScene:[LoadingScene scene]];
    NSThread* thread = [[NSThread alloc] initWithTarget:self selector:@selector(finishLoading) object:nil];
    [thread start];
}

-(void)finishLoading
{
    EAGLContext *k_context = [[[EAGLContext alloc]
                               initWithAPI :kEAGLRenderingAPIOpenGLES1
                               sharegroup:[[[[CCDirector sharedDirector] openGLView] context] sharegroup]] autorelease];
    [EAGLContext setCurrentContext:k_context];
    
    
	// Create a puzzle
    CCScene *scene = [PuzzleScene scene];
    
    [[CCDirector sharedDirector] popScene];
    
    [[CCDirector sharedDirector] pushScene:scene];
	//[[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:scene]];
    
    PuzzleScene *layer = (PuzzleScene *) [scene.children objectAtIndex:0];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate addGestureRecognizersTo:layer];
}


-(NSString *)currentProductName
{
    return [self.currentProductPath lastPathComponent];
}

-(NSString *)currentProductNameLevel
{
    if(currentLevel)
    {
        return [[self currentProductName] stringByAppendingFormat:@"-%@",currentLevel];
    } else {
        return [self currentProductName];
    }
}

-(void)resetCurrentProductPath
{
    self.currentProductPath = nil;
    currentLevel = nil;
}

- (UIImage *)thumbnailFromProductPath:(NSString *)productPath width:(int)width {
    NSString *productName = [productPath lastPathComponent];
    NSString *filename = [Utils filenameWithBase:@"thumbnail" andExtension:@"png" forWriting:YES];
    NSString *pathToThumbImage = [productPath stringByAppendingPathComponent:filename];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:pathToThumbImage]) 
    {
#if TARGET_IPHONE_SIMULATOR
        pathToThumbImage = [ModelLoader storingPath:productName level:nil filename:filename];
#else
        pathToThumbImage = [[productPath stringByAppendingPathComponent:@"generated"] stringByAppendingPathComponent:filename];
        return [UIImage imageWithContentsOfFile:pathToThumbImage];
#endif
        NSLog(@"Generating thumbnail for %@",productName);
        filename = [Utils filenameWithBase:@"background" andExtension:@"png" forWriting:NO];
        NSString *sourceImagePath = [productPath stringByAppendingPathComponent:filename];
        UIImage *sourceImage = [UIImage imageWithContentsOfFile:sourceImagePath];
        
        CGSize size = CGSizeMake(width, width);
        UIImage *thumbnail = [self resizeImage:sourceImage newSize:size];
        NSData *imageData = UIImagePNGRepresentation(thumbnail);
        [imageData writeToFile:pathToThumbImage atomically:YES];
    }
    return [UIImage imageWithContentsOfFile:pathToThumbImage];
}

- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);  
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();    
    
    return newImage;
}

// Cleanup and create directory
+ (NSString *)createDirectoryForProduct:(NSString *)productName level:(NSNumber *)level
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString * directoryPath = [[[documentsPath stringByAppendingPathComponent:@"products"] stringByAppendingPathComponent:productName] stringByAppendingPathComponent:@"generated"];
    if(level)
    {
        directoryPath = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",level]];   
    }
//    if([fileManager fileExistsAtPath:directoryPath])
//    {
//        [fileManager removeItemAtPath:directoryPath error:&error];
//    }
    if(![fileManager fileExistsAtPath:directoryPath])
    {
        BOOL result = [fileManager createDirectoryAtPath:directoryPath withIntermediateDirectories:YES attributes:nil error:&error];
        if(!result || ![fileManager fileExistsAtPath:directoryPath])
        {
            Log(@"Error creating directory %@ : %@", directoryPath, error);
        }
    }
    return directoryPath;
}


@end

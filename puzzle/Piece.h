//
//  Piece.h
//  puzzle
//
//  Created by Bertrand Clarke on 25/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "PieceSprite.h"

@interface Piece : NSObject
{
    // Piece without border
    CCSprite *originalSprite;
    PieceSprite *sprite;
    CCSprite *zoomedSprite;
    NSArray *color;
    BOOL small;
}

@property (nonatomic, retain) CCSprite *originalSprite;
@property (nonatomic, retain) PieceSprite *sprite;
@property (nonatomic, retain) CCSprite *zoomedSprite;
@property (nonatomic, retain) NSArray *color;
@property BOOL small;

@end

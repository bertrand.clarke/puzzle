//
//  Utils.m
//  puzzle
//
//  Created by Bertrand Clarke on 23/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Utils.h"
#import <CommonCrypto/CommonDigest.h>

@implementation Utils

static BOOL retinaDisplay;

+(void)log:(char*)sourceFile lineNumber:(int)lineNumber format:(NSString*)format, ...;
{
	va_list ap;
	va_start(ap,format);
	NSString *file=[[NSString alloc] initWithBytes:sourceFile 
          length:strlen(sourceFile) 
        encoding:NSUTF8StringEncoding];
	NSString *print=[[NSString alloc] initWithFormat:format arguments:ap];
	va_end(ap);
	NSLog(@"%s:%d %@",[[file lastPathComponent] UTF8String],
          lineNumber,print);
	
	return;
}

+ (void)logError:(NSError *)error
{
    Log(@"%@, %@", error, [error userInfo]);
}

+ (BOOL)iPad
{
    return [[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPhone;
}

+ (void)setRetinaDisplay:(BOOL)retina
{
    retinaDisplay = retina;
}

+ (BOOL)retinaDisplay
{
    return retinaDisplay;
}

+(NSString *)filenameWithBase:(NSString *)baseFilename andExtension:(NSString *)extension forWriting:(BOOL)writing
{
    NSString *filename = baseFilename;
    if([Utils iPad])
    {
        filename = [filename stringByAppendingString:@"-iPad"];
    }
    if([Utils retinaDisplay] && writing)
    {
        filename = [filename stringByAppendingString:@"-hd"];
    }
    filename = [filename stringByAppendingPathExtension:extension];
    return filename;
}

@end

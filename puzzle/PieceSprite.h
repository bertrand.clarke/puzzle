//
//  PieceSprite.h
//  puzzle
//
//  Created by Bertrand Clarke on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

@interface PieceSprite : CCSprite

-(void) addShadowWithColor:(UIColor*)shadowColor withBlur:(float)blur withOffset:(CGPoint)offset;
@end

//
//  HelloWorldLayer.m
//  puzzle
//
//  Created by Bertrand Clarke on 23/05/12.
//  Copyright Bcdlog 2012. All rights reserved.
//

//zorders
// 0 background (could be pieces)
// 1 placed pieces
// 2, 3 pieces

// Import the interfaces
#import "PuzzleScene.h"
#import "Piece.h"
#import "Utils.h"
#import "ModelLoader.h"
#import "AppDelegate.h"
#import "MainMenuScene.h"
#import "Parameters.h"
#import "SimpleAudioEngine.h"
#import "PieceSprite.h"

// 10 is harder
#define MIN_DISTANCE_TO_MATCH 20
#define MARGIN_LEFT 10

// HelloWorldLayer implementation
@implementation PuzzleScene

static int buttonsWidth;

@synthesize backgroundSprite, gridSprite,finishSprite,pieces;

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	PuzzleScene *layer = [PuzzleScene node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(void)loadPieces
{
    ModelLoader *modelLoader = [[ModelLoader alloc] init];
    [modelLoader loadModelWithController:self];
    
    currentZOrder = pieces.count;

}

-(void)showGrid:(id)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"click.mp3"];
    gridSprite.visible = !gridSprite.visible;
}

-(void)showBackground:(id)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"click.mp3"];
    finishSprite.visible = !finishSprite.visible;
}

-(void)rotate:(id)sender
{
    [[SimpleAudioEngine sharedEngine] playEffect:@"click.mp3"];
    NSArray *piecesCopy = [pieces copy];
    for(Piece *piece in piecesCopy)
    {
        piece.sprite.rotation = 0;
        [self checkMatch:piece];
    }

}

// on "init" you need to initialize your instance
-(id) init
{
	if( (self=[super init])) {

        [Parameters loadParametersFromFile];
        zoomEnabled = YES;
        
        pieces = [[NSMutableArray alloc] init];
        winSize = [CCDirector sharedDirector].winSize;
        
        CCMenuItemImage *exitMenuItem = [CCMenuItemImage itemFromNormalImage:@"exit.png" selectedImage:@"exitSelected.png" 
                                                                      target:self selector:@selector(exit:)];
        buttonsWidth = exitMenuItem.boundingBox.size.width * 2;
        //float y  = winSize.height - (exitMenuItem.boundingBox.size.height / 2);
//        y = 70.0;
//        buttons.position = ccp(MARGIN_LEFT + (exitMenuItem.boundingBox.size.width / 2),y);
//        NSLog(@"h=%f",y);
        // Add top left return button 
        
        CCMenuItemImage *showGrid = [CCMenuItemImage itemFromNormalImage:@"ShowGrid.png"
                                                         selectedImage:@"ShowGrid.png"
                                                                target:nil
                                                              selector:nil];
        CCMenuItemImage *hideGrid = [CCMenuItemImage itemFromNormalImage:@"HideGrid.png"
                                                           selectedImage:@"HideGrid.png"
                                                                  target:nil
                                                                selector:nil];
        CCMenuItemToggle *gridMenuItem = [CCMenuItemToggle itemWithTarget:self selector:@selector(showGrid:) items:showGrid, hideGrid, nil];
                                          
        if([Parameters showGrid])
        {
            [gridMenuItem setSelectedIndex:1];
            
        }
        CCMenuItemImage *showBackground = [CCMenuItemImage itemFromNormalImage:@"ShowBackground.png"
                                                           selectedImage:@"ShowBackground.png"
                                                                  target:nil
                                                                selector:nil];
        CCMenuItemImage *hideBackground = [CCMenuItemImage itemFromNormalImage:@"HideBackground.png"
                                                           selectedImage:@"HideBackground.png"
                                                                  target:nil
                                                                selector:nil];
        CCMenuItemToggle *backgroundMenuItem = [CCMenuItemToggle itemWithTarget:self selector:@selector(showBackground:) items:showBackground, hideBackground, nil];

        if([Parameters showBackground])
        {
            [backgroundMenuItem setSelectedIndex:1];
        }


        CCMenuItemImage *rotateMenuItem = [CCMenuItemImage itemFromNormalImage:@"rotate.png" selectedImage:@"rotateSelected.png" 
                                                                      target:self selector:@selector(rotate:)];
        
        timerLabel = [CCLabelTTF labelWithString:@"00:00" fontName:@"Marker Felt" fontSize:20];
       
        CCMenu * buttons = [CCMenu menuWithItems:exitMenuItem,gridMenuItem,backgroundMenuItem,rotateMenuItem,nil];
        buttons.position = ccp(MARGIN_LEFT + (exitMenuItem.boundingBox.size.width / 2),(winSize.height / 2) + (timerLabel.boundingBox.size.height / 2));
        [buttons alignItemsVertically];
        [self addChild:buttons];
        
        //[self performSelectorInBackground:@selector(loadPieces) withObject:nil];
        [self loadPieces];
        
		timerLabel.position =  ccp(MARGIN_LEFT + timerLabel.boundingBox.size.width / 2 , timerLabel.boundingBox.size.height / 2 + 10.0);
		[self addChild: timerLabel];
        [self schedule: @selector(tick:) interval:1.0];
        self.isTouchEnabled = YES;
	}
	return self;
}

-(void)exit:(id)sender
{
    [[MainMenuScene mainMenuScene] resetCurrentProductPath];
    [[SimpleAudioEngine sharedEngine] playEffect:@"click.mp3"];
    [[CCDirector sharedDirector] popScene];
}


-(void)tick:(ccTime)dt
{
    if([pieces count] > 0)
    {
        ++ellapsedSeconds;
        int minutes = ellapsedSeconds / 60;
        int seconds = ellapsedSeconds %  60;
        timerLabel.string = [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    }
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint touchLocation = [self convertTouchToNodeSpace: [touches anyObject]];    
    [self selectSpriteForTouch:touchLocation exact:YES];
}

- (void)checkMatch:(Piece *)piece
{
    CGPoint position = piece.sprite.position;
    CGPoint originalPosition = piece.originalSprite.position;
    NSLog(@"CheckMatch position=%@ originalPosition=%@ ",NSStringFromCGPoint(position),NSStringFromCGPoint(originalPosition));
//    originalPosition.x = originalPosition.x * (1.0 + piece.originalSprite.scale);
//    originalPosition.y = originalPosition.y * (1.0 + piece.originalSprite.scale);

    int rotation = abs((int)piece.sprite.rotation % 180);
    NSLog(@"%i %@ %@ ",rotation,NSStringFromCGPoint(position),NSStringFromCGPoint(originalPosition));
    
    if((rotation < MIN_DISTANCE_TO_MATCH || (rotation > 180 - MIN_DISTANCE_TO_MATCH))
       && position.x > originalPosition.x - MIN_DISTANCE_TO_MATCH
       && position.x < originalPosition.x + MIN_DISTANCE_TO_MATCH 
       && position.y > originalPosition.y - MIN_DISTANCE_TO_MATCH 
       && position.y < originalPosition.y + MIN_DISTANCE_TO_MATCH) 
    {
        [pieces removeObject:piece];
        piece.sprite.visible = NO;
        
        //selectedPiece.originalSprite.position = originalPosition;
        [self addChild:piece.originalSprite z:1];
        
        
        [[SimpleAudioEngine sharedEngine] playEffect:@"swap.mp3"];
        if([pieces count] == 0) 
        {
            [[SimpleAudioEngine sharedEngine] playEffect:@"applause.mp3"];
            backgroundSprite.visible = NO;
            gridSprite.visible = NO;
            finishSprite.visible = YES;
            finishSprite.opacity = 255;            
        }
    } else {
        //[self drawShadow];
        //[piece.sprite addShadowWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5] withBlur:5.0f withOffset:CGPointMake(5.0f, -5.0f)];
    }

}

-(bool) isTransparentWithSprite: (CCSprite *)sprite pointInNodeSpace: (CGPoint) point {
	bool isTransparent = YES;
	// see if the point is transparent. if so, ignore it.
	// http://www.cocos2d-iphone.org/forum/topic/18522
	unsigned int w = 1;
	unsigned int h = 1;
    
	//unsigned int wh = w * h;
    ccColor4B *buffer = malloc( sizeof(ccColor4B));
	// Draw into the RenderTexture
    CCRenderTexture* rt = [CCRenderTexture renderTextureWithWidth:sprite.boundingBox.size.width  height:sprite.boundingBox.size.height];
	[rt beginWithClear:0 g:0 b:0 a:0];
    
	// move touch point to 0,0 so it goes to the right place on the rt
	// http://www.cocos2d-iphone.org/forum/topic/18796
	// hold onto old position to move it back immediately
	CGPoint oldPos = sprite.position;
	CGPoint oldAnchor = sprite.anchorPoint;
    float oldRotation = sprite.rotation;
	sprite.anchorPoint = ccp(0, 0);
	sprite.position = ccp(-point.x, -point.y);
    sprite.rotation = 0;
	[sprite visit];
	sprite.anchorPoint = oldAnchor;
	sprite.position = oldPos;
    sprite.rotation = oldRotation;
	// read the pixels
	float x = 0;//local.x;
	float y = 0;//local.y;
	glReadPixels(x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
	//NSLog(@"%d: (%f, %f)", sprite.tag, x, y);
	[rt end];
    
	// Read buffer
	ccColor4B color = buffer[0];
    isTransparent = (color.a == 0);
	free(buffer);
	return isTransparent;
}

-(void)scaleUp:(CGFloat)scale
{
    if(zoomEnabled && selectedPiece.zoomedSprite)
    {
        [selectedPiece.sprite runAction:[CCScaleBy actionWithDuration: 0.15 scale: scale]];
        selectedPiece.zoomedSprite.position = selectedPiece.sprite.position;
        selectedPiece.zoomedSprite.visible = YES;
        [self reorderChild:selectedPiece.zoomedSprite z:selectedPiece.sprite.zOrder + 1];
        selectedPiece.sprite.visible = NO;
    }
}

-(void)scaleDown
{
    if(zoomEnabled && selectedPiece.zoomedSprite && !selectedPiece.sprite.visible)
    {
        selectedPiece.zoomedSprite.visible = NO;
        selectedPiece.sprite.visible = YES;
        [selectedPiece.sprite runAction:[CCScaleTo actionWithDuration: 0.15 scale: 1.0]];
    }
}

// Exact : Select only if touch is into the shape
- (void)selectSpriteForTouch:(CGPoint)touchLocation exact:(BOOL)exact{
    Piece * newPiece = nil;
    int maxZorder = 0;
    for (Piece *piece in pieces) {
        if (CGRectContainsPoint(piece.sprite.boundingBox, touchLocation)) {                       
//            NSLog(@"piece.sprite.boundingBox.origin=%@ %@ piece.sprite.boundingBox.size=%@",NSStringFromCGPoint(piece.sprite.boundingBox.origin),NSStringFromCGPoint(touchLocation),NSStringFromCGSize(piece.sprite.boundingBox.size));
//            NSLog(@"selectedPiece : %@ %@ %@",[newPiece.color objectAtIndex:0],[newPiece.color objectAtIndex:1],[newPiece.color objectAtIndex:2]);
            CGPoint local = [piece.sprite convertToNodeSpace:touchLocation];
            if((piece.small || !exact || ![self isTransparentWithSprite:piece.sprite pointInNodeSpace:local]) && piece.sprite.zOrder > maxZorder)
            {
                maxZorder = piece.sprite.zOrder;
                newPiece = piece;
            }
        }
    }
    if(newPiece) 
    {
        NSLog(@"newPiece : %@ %@ %@ %@",[newPiece.color objectAtIndex:0],[newPiece.color objectAtIndex:1],[newPiece.color objectAtIndex:2],NSStringFromCGRect(newPiece.sprite.boundingBox));
        [[SimpleAudioEngine sharedEngine] playEffect:@"click.mp3"];
        if (newPiece != selectedPiece) {
            ++currentZOrder;
            [self reorderChild:newPiece.sprite z:currentZOrder];
            selectedPiece = newPiece;
        }

        // Pb with rotation
        // Shift piece beyond finger
//        if(zoomEnabled && selectedPiece.small)
//        {
//            CGPoint position = selectedPiece.sprite.position;
//            position.x = touchLocation.x + 20 + (selectedPiece.sprite.boundingBox.size.width / 2);
//            [self setSpritePosition:selectedPiece.sprite position:position];
//        }
        
        // Hide other pieces
        //[self hidePieces];
        
//        if(newPiece.small)
//        {
//            [self scaleUp:3.0];
//            NSLog(@"Small piece");
//        }

    } else {
        selectedPiece = nil;
    }
}

-(void)hidePieces
{
    for(Piece *piece in pieces)
    {
        if(piece != selectedPiece)
        {
            piece.sprite.visible = NO;
        } else {
            piece.sprite.visible = YES;
        }
    }
}

-(void)showPieces
{
    for(Piece *piece in pieces)
    {
        piece.sprite.visible = YES;
    }
}

-(BOOL)isInside:(CGPoint)position
{
    return position.x > 0 && position.x < winSize.width && position.y > 0 && position.y < winSize.height;
}

-(void)setSpritePosition:(CCSprite *)sprite position:(CGPoint)newPos
{
    if([self isInside:newPos])
    {
        sprite.position = newPos;
    }    
}

-(void) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(selectedPiece)
    {
        NSLog(@"ccTouchesEnded");
        [self scaleDown];
        //[self showPieces];
    }
}

- (void)handleRotateFrom:(UIRotationGestureRecognizer *)recognizer 
{     
    if(selectedPiece)
    {
        if (recognizer.state == UIGestureRecognizerStateChanged) {
            float rotation = CC_RADIANS_TO_DEGREES([recognizer rotation]);
            selectedPiece.sprite.rotation += rotation;
            selectedPiece.zoomedSprite.rotation += rotation;
            [recognizer setRotation:0];
        } else if(recognizer.state == UIGestureRecognizerStateEnded) {
            [self scaleDown];
            [self checkMatch:selectedPiece];
            //[self showPieces];
        }
    }
}

- (void)handlePanFrom:(UIPanGestureRecognizer *)recognizer {
    
    if(selectedPiece)
    {
        CGPoint translation = [recognizer translationInView:recognizer.view];
        translation = ccp(translation.x, -translation.y);
        CGPoint newPos = ccpAdd(selectedPiece.sprite.position, translation);
        if (recognizer.state == UIGestureRecognizerStateChanged) {    

            //NSLog(@"selectedPiece moved from %f %f to : %f %f",selectedPiece.sprite.position.x,selectedPiece.sprite.position.y,newPos.x, newPos.y);
            
            [self setSpritePosition:selectedPiece.sprite position:newPos];
            [self setSpritePosition:selectedPiece.zoomedSprite position:newPos];
            [recognizer setTranslation:CGPointZero inView:recognizer.view];
        } else if (recognizer.state == UIGestureRecognizerStateEnded) {
            [self scaleDown];
            [self checkMatch:selectedPiece];
//            CGRect box = selectedPiece.sprite.boundingBox;
//            float halfWidth = box.size.width / 2;
//            float halfHeight = box.size.height / 2;
//            if(newPos.x >  halfWidth
//               && newPos.x < winSize.width - halfWidth
//               && newPos.y > halfHeight
//               && newPos.y < winSize.height - halfHeight)
//            {
                //[self showPieces];
//            }
        }        
    }
}

//- (void)handlePinchFrom:(UIPinchGestureRecognizer *)recognizer {
//    if(selectedPiece)
//    {
//        if (recognizer.state == UIGestureRecognizerStateBegan)
//        {
//            //float zoomScale = [recognizer scale];
//            [self scaleUp:[recognizer scale]];
//        } else if (recognizer.state == UIGestureRecognizerStateEnded) {
//            [self scaleDown];
//            [self showPieces];
//        }
//    }
//}

//- (void)handleLongPressFrom:(UILongPressGestureRecognizer *)recognizer {
//    
//    if(selectedPiece)
//    {
//        if (recognizer.state == UIGestureRecognizerStateBegan)
//        {
//            [self scaleUp];
//        } else if (recognizer.state == UIGestureRecognizerStateEnded) {
//            [self scaleDown];
//            [self showPieces];
//        } 
//    }
//}

+(int)buttonsWidth
{
    return buttonsWidth;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end

//
//  Parameters.h
//  puzzle
//
//  Created by Bertrand Clarke on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parameters : NSObject

+(void)loadParametersFromFile;
+(unsigned int)borderWidth;
+(CGColorRef)borderColor;
+(NSNumber *)minPointsPerPiece;
+(BOOL)showBackground;
+(BOOL)showGrid;
+(BOOL)rotatePieces;
@end

//
//  MainMenuScene.h
//  puzzle
//
//  Created by Bertrand Clarke on 05/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"

@interface MainMenuScene : CCLayer
{
    
    NSMutableArray *productsDirectories;
    NSString *currentProductPath;
    NSMutableArray *levels;
    NSNumber *currentLevel;
}

@property (nonatomic, strong) NSMutableArray *productsDirectories;
@property (nonatomic, strong) NSString *currentProductPath;
@property (nonatomic, strong) NSMutableArray *levels;
@property (nonatomic, strong) NSNumber * currentLevel;

+(CCScene *) scene;
+(MainMenuScene *)mainMenuScene;
+ (NSString *)createDirectoryForProduct:(NSString *)productName level:(NSNumber *)level;

-(void)load;
-(NSString *)currentProductName;
-(NSString *)currentProductNameLevel;
-(void)resetCurrentProductPath;

@end

//
//  BorderPoint.h
//  puzzle
//
//  Created by Bertrand Clarke on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BorderPoint : NSObject
{
    BOOL isTopBorder;
    BOOL isBottomBorder;
    BOOL isLeftBorder;
    BOOL isRightBorder;
    int row;
    int col;
}

@property BOOL isTopBorder;
@property BOOL isBottomBorder;
@property BOOL isLeftBorder;
@property BOOL isRightBorder;
@property int row;
@property int col;


@end

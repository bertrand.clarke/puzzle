//
//  PieceSprite.m
//  puzzle
//
//  Created by Bertrand Clarke on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PieceSprite.h"

@implementation PieceSprite

//-(void)draw
//{
//    // is_shadow is true if this sprite is to be considered like a shadow sprite, false otherwise.@
////    if (is_shadow)
////    {
//        ccBlendFunc    blend;
//        // Change the default blending factors to this one.
//        blend.src = GL_ONE;
//        blend.dst = GL_SRC_ALPHA;
//        [self setBlendFunc: blend];
//        // Change the blending equation to thi in order to subtract from the values already written in the frame buffer
//        // the ones of the sprite.
//        glBlendEquationOES(GL_FUNC_SUBTRACT_OES);
////    }
//    
//    [super draw];
//    
////    if (is_shadow)
////    {
//        // The default blending function of cocos2d-x is GL_FUNC_ADD.
//        glBlendEquationOES(GL_FUNC_ADD_OES);        
////    }
//}





-(void) addShadowWithColor:(UIColor*)shadowColor withBlur:(float)blur withOffset:(CGPoint)offset
{
//    UIImage *imageFromSprite = [self convertSpriteToImage:self];
//    UIImage *imageWithShadow = [self imageWithShadow:imageFromSprite withColor:shadowColor withBlur:blur withOffset:offset];
//    
//    //CCTexture2D *imageWithShadowTexture = [[CCTexture2D alloc] initWithImage:imageWithShadow];
//    CCTexture2D *imageWithShadowTexture = [[CCTexture2D alloc] initWithImage:imageFromSprite];
//    
//    [self setTexture:imageWithShadowTexture];
//}
//
//- (UIImage*)convertSpriteToImage:(CCSprite *)convertSprite {
//    CGPoint p = convertSprite.anchorPoint;
//    [convertSprite setAnchorPoint:ccp(0,0)];
//    
//    CCRenderTexture *renderer = [CCRenderTexture renderTextureWithWidth:convertSprite.contentSize.width height:convertSprite.contentSize.height];
//    
//    [renderer begin];
//    [convertSprite visit];
//    [renderer end];
//    
//    [convertSprite setAnchorPoint:p];
//    
//    UIImage *newImage = [UIImage imageWithData:[renderer getUIImageAsDataFromBuffer:kCCImageFormatPNG]];
//    
//    return newImage;
}

- (UIImage*)imageWithShadow:(UIImage*)image withColor:(UIColor*)shadowColor withBlur:(float)blur withOffset:(CGPoint)offset
{
//    CGColorSpaceRef colourSpace = CGColorSpaceCreateDeviceRGB();
//    CGContextRef shadowContext = CGBitmapContextCreate(NULL, image.size.width, image.size.height, CGImageGetBitsPerComponent(image.CGImage), 0,
//                                                       colourSpace, kCGImageAlphaPremultipliedLast);
//    CGColorSpaceRelease(colourSpace);
//    
//    CGContextSetShadowWithColor(shadowContext, CGSizeMake(offset.x, offset.y), blur, shadowColor.CGColor);
//    
//    CGContextDrawImage(shadowContext, CGRectMake(0, 10, image.size.width, image.size.height), image.CGImage);
//    CGImageRef shadowedCGImage = CGBitmapContextCreateImage(shadowContext);
//    CGContextRelease(shadowContext);
//    
//    UIImage * shadowedImage = [UIImage imageWithCGImage:shadowedCGImage];
//    CGImageRelease(shadowedCGImage);
//    
//    return shadowedImage;
    return image;
}

@end

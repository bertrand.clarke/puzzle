//
//  HelloWorldLayer.h
//  puzzle
//
//  Created by Bertrand Clarke on 23/05/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
@class Piece;

// HelloWorldLayer
@interface PuzzleScene : CCLayer
{
    CGSize winSize;
    CCSprite * backgroundSprite;
    CCSprite * gridSprite;
    CCSprite *finishSprite;

    Piece *selectedPiece;
    NSMutableArray *pieces;
    CCLabelTTF *timerLabel;
    int ellapsedSeconds;
    BOOL zoomEnabled;
    int currentZOrder;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;
+(int)buttonsWidth;

@property (nonatomic, retain) CCSprite * backgroundSprite;
@property (nonatomic, retain) CCSprite * gridSprite;
@property (nonatomic, retain) CCSprite * finishSprite;
@property (nonatomic, retain) NSMutableArray *pieces;

@end

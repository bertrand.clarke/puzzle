//
//  ModelLoader.m
//  puzzle
//
//  Created by Bertrand Clarke on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ModelLoader.h"
#import "Piece.h"
#import "Border.h"
#import "Parameters.h"
#import "MainMenuScene.h"
#import "Utils.h"
#import "PieceSprite.h"

#define MIN_WIDTH 50
#define MIN_HEIGHT 50

@implementation ModelLoader

- (void) positionInTheMiddle:(CCSprite *)sprite
{
    int buttonsWidth = [PuzzleScene buttonsWidth];
    float x = ((winSize.width - buttonsWidth) / 2) + buttonsWidth;
    float y = winSize.height / 2;
    CGPoint origin = CGPointMake(x, y);
    sprite.position = [[CCDirector sharedDirector] convertToGL:origin];
}

-(BOOL)isGrid
{
    return pieceRect.size.width == imageWidth && pieceRect.size.height == imageHeight;
}

-(void)createBackgroundAndFinishSprites
{
//    EAGLContext *auxGLcontext = [[EAGLContext alloc]
//                    initWithAPI:kEAGLRenderingAPIOpenGLES1
//                    sharegroup:[[[[CCDirector sharedDirector] openGLView] context] sharegroup]];
//    [EAGLContext setCurrentContext:auxGLcontext];
    NSString *baseFilePath = [[MainMenuScene mainMenuScene].currentProductPath stringByAppendingPathComponent:@"background"];
    NSString *filePath = [Utils filenameWithBase:baseFilePath andExtension:@"png" forWriting:YES];
    NSLog(@"filePath=%@",filePath);
    CGDataProviderRef backgroundProvider = CGDataProviderCreateWithFilename([filePath UTF8String]);
    backgroundRef = CGImageCreateWithPNGDataProvider(backgroundProvider, NULL, NO, kCGRenderingIntentDefault);
    imageWidth = CGImageGetWidth(backgroundRef);
    NSLog(@"background width=%zul",CGImageGetWidth(backgroundRef));
    imageHeight = CGImageGetHeight(backgroundRef);
    //scale = winSize.height / imageHeight * 2;

    NSLog(@"background height=%zul",CGImageGetHeight(backgroundRef));
    bitsPerComponent = CGImageGetBitsPerComponent(backgroundRef);
    //NSLog(@"bitsPerComponent=%zul",CGImageGetBitsPerComponent(backgroundRef));
    bytesPerRow = CGImageGetBytesPerRow(backgroundRef);
    //NSLog(@"bytesPerRow=%zul",CGImageGetBytesPerRow(backgroundRef));
    colorSpace = CGImageGetColorSpace(backgroundRef);
    //NSLog(@"colorSpace=%@",CGImageGetColorSpace(backgroundRef));
    bytesPerPixel = bytesPerRow / imageWidth;
    //NSLog(@"bytesPerPixel=%u",bytesPerPixel);
    
    [self createRectangleWithColor:1.0 green:1.0 blue:1.0];

    // Create background white rectangle sprite
    controller.backgroundSprite = [CCSprite spriteWithCGImage:currentImageRef key:nil];
    [self positionInTheMiddle:controller.backgroundSprite];
    //controller.backgroundSprite.scale = scale;
    [controller addChild:controller.backgroundSprite z:0];
    
    // Create background image sprite
    filePath = [Utils filenameWithBase:baseFilePath andExtension:@"png" forWriting:NO];
    controller.finishSprite = [CCSprite spriteWithFile:filePath];
    [self positionInTheMiddle:controller.finishSprite];
    //controller.finishSprite.scale = scale;
    controller.finishSprite.opacity = 150;
    controller.finishSprite.visible = [Parameters showBackground];
    [controller addChild:controller.finishSprite];
    
    CGDataProviderRelease(backgroundProvider);
    CGImageRelease(currentImageRef);


//    [EAGLContext setCurrentContext:nil];
}

-(void)createGridFromFile:(NSString *)filePath
{
//    EAGLContext *auxGLcontext = [[EAGLContext alloc]
//                                 initWithAPI:kEAGLRenderingAPIOpenGLES1
//                                 sharegroup:[[[[CCDirector sharedDirector] openGLView] context] sharegroup]];
//    [EAGLContext setCurrentContext:auxGLcontext];
    // Create grid
    controller.gridSprite = [CCSprite spriteWithFile:filePath];
    controller.gridSprite.visible = [Parameters showGrid];

    [self positionInTheMiddle:controller.gridSprite];
    [controller addChild:controller.gridSprite];
//    [EAGLContext setCurrentContext:nil];
}

//-(CGRect)scaledPieceRect
//{
//    return CGRectMake(0,0,scale * pieceRect.size.width,scale * pieceRect.size.height);
//}

-(void)createPieceSprite
{
    //NSLog(@"createPieceSprite");
    // Points to pixels
    float factor = 1.0;
    if([Utils retinaDisplay])
    {
        factor = 2.0;
    }
    
    Piece *piece = [[Piece alloc] init ];
    piece.originalSprite = [CCSprite spriteWithFile:pieceImagePath];
        
    int buttonsWidth = [PuzzleScene buttonsWidth];
    PieceSprite *sprite = [PieceSprite spriteWithFile:pieceWithBorderImagePath];
    
    CGSize sizeInPoints = CGSizeMake(pieceRect.size.width / factor, pieceRect.size.height / factor);
    int xLimit = winSize.width * 2 / 3;
    int x = arc4random() % ((int)winSize.width - xLimit - (int)sizeInPoints.width) + xLimit ;
    if(x < 0 || x > winSize.width)
    {
        x = winSize.width / 2;
    }
    int yLimit = 20;
    int y = arc4random() % ((int)winSize.height - yLimit - (int)sizeInPoints.height) + yLimit ;
    if(y < 0 || y > winSize.height)
    {
        y = winSize.height / 2;
    }
    sprite.position = ccp( x, y );
    NSLog(@"Random position=%@",NSStringFromCGPoint(sprite.position));
    [controller addChild:sprite z:(2 + [controller.pieces count])];
    
    piece.small = (sizeInPoints.width <= MIN_WIDTH || sizeInPoints.height <= MIN_HEIGHT);
    
    piece.sprite = sprite;
    piece.color = [[NSArray alloc] initWithObjects:[NSNumber numberWithUnsignedChar:pieceRed], [NSNumber numberWithUnsignedChar:pieceGreen], [NSNumber numberWithUnsignedChar:pieceBlue],nil];
    


    
    buttonsWidth = buttonsWidth * factor;
    //NSLog(@"buttonsWidth=%i",buttonsWidth);
    CGSize winSizePixels = CGSizeMake(winSize.width * factor, winSize.height * factor);
    //NSLog(@"winSize=%@",NSStringFromCGSize(winSizePixels));
    //NSLog(@"imageWidth=%zu imageHeight=%zu",imageWidth,imageHeight);
    float xShift = buttonsWidth + (((winSizePixels.width - buttonsWidth) - imageWidth) / 2.0);
    //NSLog(@"xShift=%f",xShift);
    float yShift = (winSizePixels.height - imageHeight) / 2.0;
    //NSLog(@"yShift=%f",yShift);
    //NSLog(@"pieceRect=%@",NSStringFromCGRect(pieceRect));
    CGPoint lowerLeft = CGPointMake((xShift + pieceRect.origin.x) / factor, (yShift + pieceRect.origin.y + pieceRect.size.height) / factor);
    lowerLeft = [[CCDirector sharedDirector] convertToGL:lowerLeft];
    piece.originalSprite.position = ccp(lowerLeft.x + (sizeInPoints.width / 2.0), lowerLeft.y + (sizeInPoints.height / 2.0));
    //NSLog(@"piece.originalSprite.position=%@",NSStringFromCGPoint(piece.originalSprite.position));
    
    if([Parameters rotatePieces])
    {
        sprite.rotation = arc4random() % 180;
    }

//    piece.zoomedSprite = [CCSprite spriteWithCGImage:zoomedPieceImageRef key:nil];
////    CGImageRelease(zoomedPieceImageRef);
//    piece.zoomedSprite.visible = NO;
//    [controller addChild:piece.zoomedSprite];
    
    //[sprite addShadowWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5] withBlur:5.0f withOffset:CGPointMake(5.0f, -5.0f)];
    
    [controller.pieces addObject:piece];
}




-(void)createPieceFromImageForKey:(id)key
{
    //NSLog(@"createPieceFromImageForKey");
    NSNumber *number = (NSNumber *)key;
    long value = [number longValue];
    pieceRed = value ;
    pieceGreen = (value / 256) - 1;
    pieceBlue  = (value / 65536) - 1;
    if(![self isGrid])
    {
        //NSLog(@"Piece color r=%i g=%i b=%i pieceRect=%@",pieceRed, pieceGreen, pieceBlue,NSStringFromCGRect(pieceRect));
        
        // Create sprite
        [self performSelectorOnMainThread:@selector(createPieceSprite) withObject:nil waitUntilDone:YES];
    }
}

-(CGImageRef)writeImageToDisk:(CGImageRef)imageRef baseFilePath:(NSString *)baseFilePath
{
    //NSLog(@"writeImageToDisk");
    if(!imageRef)
    {
        NSLog(@"Error ");
    }
    
    NSString *filePath = [Utils filenameWithBase:baseFilePath andExtension:@"png" forWriting:YES];
    
    CGSize size = CGSizeMake(CGImageGetWidth(imageRef), CGImageGetHeight(imageRef));
    // Write piece image to disk into <key>.png
    CGContextRef context = CGBitmapContextCreate(nil,size.width,size.height,
                                                 CGImageGetBitsPerComponent(imageRef),
                                                 CGImageGetBytesPerRow(imageRef),
                                                 CGImageGetColorSpace(imageRef),
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0,0,size.width,size.height),imageRef);
    imageRef = CGBitmapContextCreateImage(context);
    if(!imageRef)
    {
        NSLog(@"Error writing %@ pieceRect=%@ r=%i g=%i b=%i",filePath,NSStringFromCGRect(pieceRect),pieceRed,pieceGreen,pieceBlue);
    }
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    NSData *imageData = UIImagePNGRepresentation(img);
    //NSLog(@"Writing %@",filePath);
    [imageData writeToFile:filePath atomically:YES];
    CGContextRelease(context);
    return imageRef;
}

-(BOOL)pixelInColor:(int)byteIndex
{
    unsigned char red = rawData[byteIndex];
    unsigned char green = rawData[byteIndex + 1];
    unsigned char blue  = rawData[byteIndex + 2];
    return red == pieceRed && green == pieceGreen && blue == pieceBlue;
}

// Create a piece from mask and background
-(void)createPieceFromColor:(id)key
{
    //NSLog(@"createPieceFromColor");
    NSNumber *number = (NSNumber *)key;
    long value = [number longValue];
    pieceRed = value ;
    pieceGreen = (value / 256) - 1;
    pieceBlue  = (value / 65536) - 1;
    
    // Create mask
    int minX = imageWidth;
    int maxX = 0;
    int minY = imageHeight;
    int maxY = 0;
    for(int row = 0; row < imageHeight ; ++row)
    {
        for(int col = 0; col < imageWidth ; ++col)
        {
            int byteIndex = ((row * imageWidth) + col) * bytesPerPixel;
            
            if ([self pixelInColor:byteIndex]) {
                // Inside piece set pixel to black to keep it
                rawData[byteIndex] = 0;
                rawData[byteIndex + 1] = 0;
                rawData[byteIndex + 2] = 0;
                
                if(col < minX) {
                    minX = col;
                }
                if(col > maxX) {
                    maxX = col;
                }
                if(row < minY) {
                    minY = row;
                }
                if(row > maxY) {
                    maxY = row;
                }
                gridData[byteIndex] = 255;
                gridData[byteIndex + 1] = 255;
                gridData[byteIndex + 2] = 255;
                gridData[byteIndex + 3 ] = 255;
            } else {
                // Outside piece set to white
                rawData[byteIndex] = 255;
                rawData[byteIndex + 1] = 255;
                rawData[byteIndex + 2] = 255;
            }
        }
    }
    
//    unsigned long *gridData = (unsigned long *)CGBitmapContextGetData (gridContextRef);
//    for (int i = 0 ; i < CGBitmapContextGetWidth(gridContextRef) * CGBitmapContextGetHeight(gridContextRef) ; ++i)
//    {
//        NSNumber *pointKey = [NSNumber numberWithUnsignedLong:gridData[i]];
//        if([pointKey isEqualToNumber:number])
//        {
//            // Make a hole into gridData in place of the piece
//            gridData[i] = 4294967295;
//        }
//    }
    
    pieceRect = CGRectMake(minX, minY, (maxX - minX) + 1, (maxY - minY) + 1);
    if(pieceRect.origin.x < 0 || pieceRect.origin.y < 0 || pieceRect.size.width < 0 || pieceRect.size.height < 0) {
        // Pieces with red=0 are ignored
        NSLog(@"Ignoring piece color r=%i g=%i b=%i pieceRect=%@ minX=%i maxX=%i minY=%i maxY=%i",pieceRed, pieceGreen, pieceBlue,NSStringFromCGRect(pieceRect),minX,maxX,minY,maxY);
        return;
    }

    // Enlarge image for borders
    int borderWidth = [Parameters borderWidth];
    if(minX - borderWidth >= 0)
    {
        minX = minX - borderWidth;
    }
    if(maxX + borderWidth < imageWidth)
    {
        maxX = maxX + borderWidth;
    }
    if(minY - borderWidth >= 0)
    {
        minY = minY - borderWidth;
    }
    if(maxY + borderWidth < imageHeight)
    {
        maxY = maxY + borderWidth;
    }
    
    // Enlarge small pieces
    int width = maxX - minX;
    if(width < MIN_WIDTH)
    {
        int diff = MIN_WIDTH - width;
        NSLog(@"Enlarge piece width by %i",diff);
        int newMinX = minX - (diff / 2);
        if(newMinX > 0)
        {
            minX = newMinX;
        } else {
            minX = 0;
        }
        int newMaxX = maxX + (diff / 2);
        if(newMaxX < imageWidth)
        {
            maxX = newMaxX;
        } else {
            maxX = imageWidth - 1;
        }
    }
    
    int height = maxY - minY;
    if(height < MIN_HEIGHT)
    {
        int diff = MIN_HEIGHT - height;
        NSLog(@"Enlarge piece height by %i",diff);
        
        int newMinY = minY - (diff / 2);
        if(newMinY > 0)
        {
            minY = newMinY;
        } else {
            minY = 0;
        }
        int newMaxY = maxY + (diff / 2);
        if(newMaxY < imageHeight)
        {
            maxY = newMaxY;
        } else {
            maxY = imageHeight - 1;
        }

    }

    
    pieceRect = CGRectMake(minX, minY, (maxX - minX) + 1, (maxY - minY) + 1);
       
    
    // Mask where pixels of the given color are replaced by black and others by white
    CGContextRef maskContext = CGBitmapContextCreate(rawData,
                                                     imageWidth,
                                                     imageHeight,
                                                     bitsPerComponent,
                                                     bytesPerRow,
                                                     colorSpace,
                                                     kCGImageAlphaPremultipliedLast ); 
    CGImageRef maskRef = CGBitmapContextCreateImage(maskContext);
    CGContextRelease(maskContext);
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    CGImageRef pieceRef = CGImageCreateWithMask(backgroundRef, mask);
    // Crop to piece rectangle
    CGImageRef imageRef = CGImageCreateWithImageInRect(pieceRef, pieceRect);

    // Save piece color and origin (upper left corner)
    [piecesColorOrigin setObject:NSStringFromCGRect(pieceRect) forKey:[NSString stringWithFormat:@"%u",[number longValue]]];

    currentImageRef = imageRef;
    
    // Create zoomed piece
    [self createZoomedPieceAndBorders:key];
    
    CGImageRelease(imageRef);
    CGImageRelease(pieceRef);
    CGImageRelease(mask);
    CGImageRelease(maskRef);

}

-(void)createZoomedPieceAndBorders:(id)key
{
    NSString* baseFilePath = [ModelLoader storingPath:[[MainMenuScene mainMenuScene] currentProductName] level:[MainMenuScene mainMenuScene].currentLevel  filename:[NSString stringWithFormat:@"%u",[key longValue]]];
//    NSString *filePath = [baseFilePath stringByAppendingString:@"-zoomed.png"];
//
//    // Draw border around piece image and write zoomed image
//    int bpr = pieceRect.size.width * bytesPerPixel;
//    CGContextRef context = CGBitmapContextCreate(nil,pieceRect.size.width,pieceRect.size.height,
//                                                 CGImageGetBitsPerComponent(currentImageRef),
//                                                 bpr,
//                                                 CGImageGetColorSpace(currentImageRef),
//                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
//    CGContextDrawImage(context, CGRectMake(0,0,pieceRect.size.width,pieceRect.size.height),currentImageRef);
//    Border *border = [[Border alloc] initWithSize:pieceRect.size andBytesPerPixel:bytesPerPixel];
//    [border drawBorderIn:context];
//    zoomedPieceImageRef = CGBitmapContextCreateImage(context);
//    [self writeImageToDisk:zoomedPieceImageRef filePath:filePath];
//    CGContextRelease(context);
        
    // Piece image
    CGSize pieceSize = pieceRect.size;
    // Draw a border around piece
    int bpr = pieceSize.width * bytesPerPixel;
    CGContextRef context = CGBitmapContextCreate(nil,pieceSize.width,pieceSize.height,
                                    CGImageGetBitsPerComponent(currentImageRef),
                                    bpr,
                                    CGImageGetColorSpace(currentImageRef),
                                    kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0,0,pieceSize.width,pieceSize.height),currentImageRef);
    currentImageRef = CGBitmapContextCreateImage(context);
    
    // Write image without border
    [self writeImageToDisk:currentImageRef baseFilePath:baseFilePath];

    // Draw border around piece image
    Border *border = [[Border alloc] initWithSize:pieceSize andBytesPerPixel:bytesPerPixel];
    [border drawBorderIn:context];
    currentImageRef = CGBitmapContextCreateImage(context);
    baseFilePath = [baseFilePath stringByAppendingString:@"-border"];
    [self writeImageToDisk:currentImageRef baseFilePath:baseFilePath];
    CGContextRelease(context);
    CGImageRelease(currentImageRef);
    
}

// Create grid from pixels of different color from pixels
- (void)createGrid:(NSString *)baseFilePath
{
    //NSLog(@"createGrid");

    CGImageRef maskRef = CGBitmapContextCreateImage(gridContextRef);
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);

    // Create a black rectangle
    CGSize size = CGSizeMake(imageWidth  , imageHeight);
    // Draw a border around piece
    int bpr = size.width * bytesPerPixel;
    CGContextRef bgContextRef = CGBitmapContextCreate(nil, size.width, size.height, bitsPerComponent, bpr, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    // drawing with a white stroke color
    CGContextSetRGBStrokeColor(bgContextRef, 0.0, 0.0, 0.0, 1.0);
    // drawing with a white fill color
    CGContextSetRGBFillColor(bgContextRef, 0.0, 0.0, 0.0, 1.0);
    // Add Filled Rectangle, 
    CGContextFillRect(bgContextRef, CGRectMake(0.0, 0.0, CGImageGetWidth(maskRef), CGImageGetHeight(maskRef)));
    CGImageRef bgImageRef = CGBitmapContextCreateImage(bgContextRef);
    CGContextRelease(bgContextRef);
    CGImageRef imageRef = CGImageCreateWithMask(bgImageRef, mask);   
    CGImageRelease(bgImageRef);

    [self writeImageToDisk:imageRef baseFilePath:baseFilePath];
       
    CGContextRelease(gridContextRef);
    CGImageRelease(maskRef);

}

-(void)createRectangleWithColor:(CGFloat)red green:(CGFloat)geen blue:(CGFloat)blue
{
    // Put white rectangle background
    CGContextRef bgContextRef = CGBitmapContextCreate(NULL, imageWidth, imageHeight, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast);
    // drawing with a white stroke color
    CGContextSetRGBStrokeColor(bgContextRef, red, geen, blue, 1.0);
    // drawing with a white fill color
    CGContextSetRGBFillColor(bgContextRef, red, geen, blue, 1.0);
    // Add Filled Rectangle, 
    CGContextFillRect(bgContextRef, CGRectMake(0.0, 0.0, imageWidth, imageHeight));
    currentImageRef = CGBitmapContextCreateImage(bgContextRef);
    CGContextRelease(bgContextRef);
}

-(NSString *)createPathFor:(NSString *)filename level:(NSNumber *)level
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* filePath;
    if(generatePieces)
    {
        filePath = [ModelLoader storingPath:[[MainMenuScene mainMenuScene] currentProductName] level:[MainMenuScene mainMenuScene].currentLevel filename:filename];
    } else {
        filePath = [[MainMenuScene mainMenuScene].currentProductPath stringByAppendingPathComponent:@"generated"];
        if(level)
        {
            filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",level]];
        }
        filePath = [filePath stringByAppendingPathComponent:filename];
        
        if(![fileManager fileExistsAtPath:filePath])
        {
            NSLog(@"Missing file : %@",filePath);
        }
    }
        
    return filePath;
}

- (void)loadModelWithController:(PuzzleScene *)ctrl
{
    generatePieces = NO;
#if TARGET_IPHONE_SIMULATOR
    NSLog(@"SIMULATOR");
    generatePieces = YES;
#endif
    
    winSize = [CCDirector sharedDirector].winSize;

    controller = ctrl;
    NSNumber *level = [MainMenuScene mainMenuScene].currentLevel;
    
    NSString *filename = [Utils filenameWithBase:@"grid" andExtension:@"png" forWriting:NO];
    NSString* gridFilePath = [self createPathFor:filename level:level];
    filename = [Utils filenameWithBase:@"pieces" andExtension:@"plist" forWriting:YES];
    NSString* piecesPlistPath = [self createPathFor:filename level:level];
    
    [self performSelectorOnMainThread:@selector(createBackgroundAndFinishSprites) withObject:nil waitUntilDone:YES];

    // Try to get pieces colors from stored datas
    NSString *piecesFilePath = [MainMenuScene mainMenuScene].currentProductPath;
    if(level)
    {
        piecesFilePath = [piecesFilePath stringByAppendingPathComponent:[NSString stringWithFormat:@"pieces-%@",level]];
    } else {
        piecesFilePath = [piecesFilePath stringByAppendingPathComponent:@"pieces"];
    }
    piecesFilePath = [Utils filenameWithBase:piecesFilePath andExtension:@"png" forWriting:YES];
    
    NSString * productFilesPath;
    if(generatePieces)
    {

        NSLog(@"First time pieces build");
        
        CGDataProviderRef piecesProvider = CGDataProviderCreateWithFilename([piecesFilePath UTF8String]);
        CGImageRef piecesRef = CGImageCreateWithPNGDataProvider(piecesProvider, NULL, NO, kCGRenderingIntentDefault);

        // Put pieces into rawData buffer
        unsigned int bitmapByteCount = imageWidth * imageHeight * bytesPerPixel;
        rawData = (unsigned char*) calloc(bitmapByteCount, sizeof(unsigned char));
        CGContextRef contextRef = CGBitmapContextCreate(rawData, imageWidth, imageHeight, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
        CGContextDrawImage(contextRef, CGRectMake(0, 0, imageWidth, imageHeight), piecesRef);
        
        // Put pieces into gridData buffer
        CGSize size = CGSizeMake(imageWidth , imageHeight);
        // Draw a border around piece
        int bpr = size.width * bytesPerPixel;
        gridData = (unsigned char*) calloc(bitmapByteCount, sizeof(unsigned char));
        gridContextRef = CGBitmapContextCreate(gridData, size.width, size.height, bitsPerComponent, bpr, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
        CGContextDrawImage(gridContextRef, CGRectMake(0, 0, size.width, size.height), piecesRef);
        
        // Get colors from mask
        // color -> NSNumber pixels count
        NSMutableDictionary *maskColorsPalette = [[NSMutableDictionary alloc] init];
        int imageSize = imageWidth * imageHeight;
        unsigned long *p = (unsigned long *)rawData;            
        for (int i = 0 ; i < imageSize ; ++i)
        {
            NSNumber *key = [NSNumber numberWithUnsignedLong:p[i]];
            NSDecimalNumber *pixels = [maskColorsPalette objectForKey:key];
            if(!pixels)
            {
                pixels = [NSDecimalNumber one];
            } else {
                pixels = [pixels decimalNumberByAdding:[NSDecimalNumber one]];
            }
            // color -> pixels count
            [maskColorsPalette setObject:pixels forKey:key];
        }
        // Create or reset storage directory for pieces
        productFilesPath = [MainMenuScene createDirectoryForProduct:[[MainMenuScene mainMenuScene] currentProductName] level:level];
        
//        if([[NSFileManager defaultManager] fileExistsAtPath:piecesPlistPath])
//        {
//            NSLog(@"Using already generated : %@", piecesPlistPath);
//            piecesColorOrigin = [[NSMutableDictionary alloc] initWithContentsOfFile: piecesPlistPath];
//        } else {
            NSLog(@"Creating new file : %@", piecesPlistPath);
            piecesColorOrigin = [[NSMutableDictionary alloc] init];
//        }

        int minPointsPerPiece = [[Parameters minPointsPerPiece] intValue];
        if([Utils iPad])
        {
            minPointsPerPiece = minPointsPerPiece * 2;
        }
        // Create pieces
        for (id key in maskColorsPalette) {
            NSDecimalNumber *pixels = [maskColorsPalette objectForKey:key];
//            NSNumber *number = (NSNumber *)key;
//            long value = [number longValue];
//            pieceRed = value ;
//            pieceGreen = (value / 256) - 1;
//            pieceBlue  = (value / 65536) - 1;
//            NSString* baseFilePath = [ModelLoader storingPath:[[MainMenuScene mainMenuScene] currentProductName] level:[MainMenuScene mainMenuScene].currentLevel  filename:[NSString stringWithFormat:@"%u",[key longValue]]];
//            NSString *filePath = [Utils filenameWithBase:baseFilePath andExtension:@"png" forWriting:YES];
            if([pixels intValue] >= minPointsPerPiece ) {
//                if(![[NSFileManager defaultManager] fileExistsAtPath:filePath])
//                {
                    CGContextDrawImage(contextRef, CGRectMake(0, 0, imageWidth, imageHeight), piecesRef);
                    [self createPieceFromColor:key];
//                } else {
//                    NSLog(@"Piece already generated : %@", filePath);
//                }
             }
        }
        // Write pieces.plist
        BOOL result = [piecesColorOrigin writeToFile:piecesPlistPath atomically:YES];
        if(!result)
        {
            NSLog(@"Failed to write piecesColorOrigin=%@ into piecesPlistPath=%@",piecesColorOrigin,piecesPlistPath);
        }
        NSLog(@"Created %i pieces", piecesColorOrigin.count);
        
        NSString *gridBasePath = [ModelLoader storingPath:[[MainMenuScene mainMenuScene] currentProductName] level:[MainMenuScene mainMenuScene].currentLevel filename:@"grid"];
        [self createGrid:gridBasePath];
        

        CGContextRelease(contextRef);
        CGDataProviderRelease(piecesProvider);
        CGImageRelease(piecesRef);
        free(rawData);
        free(gridData);

    } else {
        NSLog(@"Get pieces from stored datas");
        productFilesPath = [[MainMenuScene mainMenuScene].currentProductPath stringByAppendingPathComponent:@"generated"];
        if(level)
        {
            productFilesPath = [productFilesPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",level]];
        }
    }
    
    // Load from files
    piecesColorOrigin = [[NSMutableDictionary alloc] initWithContentsOfFile: piecesPlistPath];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //[formatter setNumberStyle:NSNumberFormatterDecimalStyle];

    for (NSString *color in piecesColorOrigin) {
        NSNumber *key = [formatter numberFromString:color];
        NSString *pieceOrigin  = [piecesColorOrigin objectForKey:color];
        NSString *filename = [Utils filenameWithBase:[NSString stringWithFormat:@"%u",[key longValue]] andExtension:@"png" forWriting:NO];
        pieceImagePath = [productFilesPath stringByAppendingPathComponent:filename];
        //NSLog(@"%@ %@",key,pieceImagePath);
        pieceRect = CGRectFromString(pieceOrigin);
        
        filename = [Utils filenameWithBase:[NSString stringWithFormat:@"%u-border",[key longValue]] andExtension:@"png" forWriting:NO];
        pieceWithBorderImagePath = [productFilesPath stringByAppendingPathComponent:filename];
        
        [self createPieceFromImageForKey:key];
    }
    if([[NSFileManager defaultManager] fileExistsAtPath:gridFilePath])
    {
        [self createGridFromFile:gridFilePath];
    } else {
        NSLog(@"Error : file not found : %@", gridFilePath);
    }
    NSLog(@"Loaded %i pieces", piecesColorOrigin.count);

    
    CGImageRelease(backgroundRef);
}

// <product>/<level>
+(NSString *)storingPath:(NSString *)productName level:(NSNumber *)level filename:(NSString *)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *storingPath = [[[documentsPath stringByAppendingPathComponent:@"products"] stringByAppendingPathComponent:productName] stringByAppendingPathComponent:@"generated"];
    if(level)
    {
        storingPath = [storingPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",level]];
    }
    return [storingPath stringByAppendingPathComponent:filename];
}

@end

//
//  DrawBorder.h
//  puzzle
//
//  Created by Bertrand Clarke on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

@interface Border : NSObject
{
    unsigned char *pieceData;
    CGSize size;
    unsigned int bytesPerPixel;
    unsigned int borderWidth;
    unsigned char *borderColor;
}
-(id)initWithSize:(CGSize)s andBytesPerPixel:(unsigned int)b;
-(void)drawBorderIn:(CGContextRef)context;
@end
